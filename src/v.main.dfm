object vMain: TvMain
  Left = 0
  Top = 0
  Caption = 'g Daily Log'
  ClientHeight = 351
  ClientWidth = 554
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 14
  object RzSplitter1: TRzSplitter
    Left = 0
    Top = 44
    Width = 554
    Height = 288
    Position = 106
    Percent = 19
    HotSpotVisible = True
    SplitterWidth = 7
    Align = alClient
    TabOrder = 0
    BarSize = (
      106
      0
      113
      288)
    UpperLeftControls = (
      ListView)
    LowerRightControls = (
      Memo)
    object ListView: TAdvListView
      Left = 0
      Top = 0
      Width = 106
      Height = 288
      Align = alClient
      Columns = <
        item
          Width = 15
        end
        item
          AutoSize = True
          Caption = #45216#51676
        end>
      HideSelection = False
      GroupView = True
      ReadOnly = True
      RowSelect = True
      SortType = stText
      TabOrder = 0
      ViewStyle = vsReport
      OnClick = ListViewClick
      FilterTimeOut = 0
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Tahoma'
      PrintSettings.Font.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Tahoma'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Tahoma'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = []
      ProgressSettings.ValueFormat = '%d%%'
      SortDirection = sdDescending
      DetailView.Font.Charset = DEFAULT_CHARSET
      DetailView.Font.Color = clBlue
      DetailView.Font.Height = -11
      DetailView.Font.Name = 'Tahoma'
      DetailView.Font.Style = []
      Version = '1.7.4.1'
      ExplicitHeight = 287
    end
    object Memo: TMemo
      Left = 0
      Top = 0
      Width = 441
      Height = 288
      Align = alClient
      ScrollBars = ssVertical
      TabOrder = 0
      OnChange = MemoChange
      OnKeyDown = MemoKeyDown
      OnKeyPress = MemoKeyPress
    end
  end
  object RzStatusBar: TRzStatusBar
    Left = 0
    Top = 332
    Width = 554
    Height = 19
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdTop, sdRight, sdBottom]
    BorderWidth = 0
    TabOrder = 1
    object RzVersionInfoStatus1: TRzVersionInfoStatus
      AlignWithMargins = True
      Left = 456
      Top = 0
      Width = 83
      Height = 19
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 15
      Margins.Bottom = 0
      Align = alRight
      AutoSize = True
      Field = vifFileVersion
      VersionInfo = RzVersionInfo1
      ExplicitLeft = 430
    end
    object StatusIme: TRzStatusPane
      Left = 423
      Top = 0
      Width = 33
      Height = 19
      Align = alRight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      AutoSize = True
      Caption = 'Eng'
      ExplicitTop = 6
    end
    object RzKeyStatus1: TRzKeyStatus
      Left = 383
      Top = 0
      Width = 40
      Height = 19
      Align = alRight
      KeyStrings.CapsLock = 'CAPS'
      KeyStrings.NumLock = 'NUM'
      KeyStrings.ScrollLock = 'SCR'
      KeyStrings.Insert = 'Ins'
      KeyStrings.Overwrite = 'Over'
      Alignment = taCenter
      AutoSize = True
      ExplicitLeft = 381
    end
  end
  object RzToolbar1: TRzToolbar
    Left = 0
    Top = 0
    Width = 554
    Height = 44
    Images = Imgs
    DisabledImages = ImgsDisabled
    RowHeight = 40
    ButtonLayout = blGlyphTop
    ButtonWidth = 60
    ButtonHeight = 40
    ShowButtonCaptions = True
    TextOptions = ttoShowTextLabels
    BorderInner = fsNone
    BorderOuter = fsGroove
    BorderSides = [sdTop]
    BorderWidth = 0
    TabOrder = 2
    ToolbarControls = (
      ButtonNew
      RzSpacer1
      RzToolButton1)
    object ButtonNew: TRzToolButton
      Left = 4
      Top = 2
      Layout = blGlyphTop
      Action = ActionNew
    end
    object RzSpacer1: TRzSpacer
      Left = 64
      Top = 10
      Grooved = True
    end
    object RzToolButton1: TRzToolButton
      Left = 72
      Top = 2
      Layout = blGlyphTop
      Action = ActionSave
    end
  end
  object ActionManager1: TActionManager
    DisabledImages = ImgsDisabled
    Images = Imgs
    Left = 352
    Top = 184
    StyleName = 'Platform Default'
    object ActionCurrentTime: TAction
      Caption = 'Current Time'
      ShortCut = 116
      OnExecute = ActionCurrentTimeExecute
    end
    object ActionNew: TAction
      Caption = #49352' '#44592#47197
      ImageIndex = 0
      ShortCut = 16462
      OnExecute = ActionNewExecute
    end
    object ActionSave: TAction
      Caption = #51200#51109
      Enabled = False
      ImageIndex = 1
      ShortCut = 16467
      OnExecute = ActionSaveExecute
    end
  end
  object Imgs: TPngImageList
    PngImages = <
      item
        Background = clWindow
        Name = 'Calendar Add-WF 16x16x0'
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA864000001454944415478DAA592A14FC35010C6EF5E97F10F20E8981862C3
          F03F50240E7441909010120C412CA8415008DC040605D5081421A4C825081018
          3645606B2502F1C4FA3E5E9794BCD1762BE14CDF7DFDF27BF7EE8EE99FC166D2
          F0067ED7ADACFC36E5E92340FCD3481D223C64D8C67413A6010158D14880203F
          399B61EAF1B9EBDA3C064884C5CBC079DDB4531598BAE9FF0164979DDB362705
          C82A3B2F20D04AF5C0244E8A651FA5300CAB52CAF06D6B41FE0950F782236D6A
          0208753ACBCCE77665EEB010A0EEF5F7F45D3B50D12A8BD236936A03E25A6FD1
          7D31C055F0A83B7D6C81BE92912A8E3E89ACBB4200ED7957A05DC13848960A50
          FBCCD65341C0E00684DB9E3BDF4EFC0DAFBF46245AD3F700E20534BCD0B7F9A4
          D42993E828A196F4F7443F659DE32D9B360139949D9972B9A6CB6FEAB406E20F
          4274D6DBA83E179AFFA4F806BA15B601AC0732270000000049454E44AE426082}
      end
      item
        Background = clWindow
        Name = 'Save-01-WF 16x16x0'
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA864000000B74944415478DA63545DF2AC849191B199818181838134F00388
          A730AA2D7DFE9D859549F45A98F81720FBFF7F0686467CBA181918EA6F454B32
          6AAD7AC9F3E7F7BFCF2003FE83044092C86C5C005DFDA801580C203616B01AA0
          BAF479033109E076B464035603480558BD804F03BA4524052236399C06C0C202
          E6578206FCFCF543F061A2E207B20C002A5ECCC0F0DF8581E15F212303F37262
          0DB0DFFF9FE5F9B317BFC1022A8B5F583032FE9FCCC8C860426244AC010081EF
          D8591400D1170000000049454E44AE426082}
      end>
    Left = 176
    Top = 88
    Bitmap = {}
  end
  object RzVersionInfo1: TRzVersionInfo
    Left = 272
    Top = 240
  end
  object TimerMemoChanged: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerMemoChangedTimer
    Left = 272
    Top = 288
  end
  object ImgsDisabled: TPngImageList
    PngImages = <
      item
        Background = clWindow
        Name = 'Calendar Add-WF 16x16x0 Disabled'
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA864000001264944415478DA6364A01030227356AD5AB53F2C2CCC115D112E
          71B001204924BE03101FC0A20E451CD9309001FF191919C102FFFFFFDF0F6323
          036471101B6800238A013081D5AB573B84868662B800591C593DDC001CCEC605
          1C300CC0E66C5C00E8857A8C304036111FD8BF7F3FCB8B172F647EFCF8F12231
          31F1074906AC5CB9B201E8D27220F305D0152240F60C5151D14AA20C006ACE01
          6A48676666F6FCF3E74F0A104F616161590F94DA4BAC01A7FFFDFBD708D4F405
          16A540FA03506A37B1063C061A90097441310322511502F179A20C00AAD90CB4
          71677878F81498FA152B5604005D524F301D00355E012A9C0B757A27109F00BA
          460728D5CCC4C414C8084A658462E0CB972F2778797915FEFEFD5B0E34400128
          F404887B812EB94054FCE303008F34B7BE152F18580000000049454E44AE4260
          82}
      end
      item
        Background = clWindow
        Name = 'Save-01-WF 16x16x0 Disabled'
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
          61000000017352474200AECE1CE9000000097048597300000EC300000EC301C7
          6FA864000000A84944415478DA635CB972650923236333030303070369E00710
          4F615CB56AD5772043342C2CEC0B90FD1F081AF1E9025A560F540BD2C703E47E
          0631FE83044092C86C5C005DFDA801580C203616B01A004C130DC42480F0F0F0
          06AC06900AB07A019F06748B480A446C72380D808505CCAF040D606565150C0C
          0CFC40AE018B8151E702C4854C4C4CCB893560FFFEFD2CAF5FBFFE0D1658B162
          8505307E2703B10929B100B4740D00E42CE116D3828E760000000049454E44AE
          426082}
      end>
    Left = 232
    Top = 88
    Bitmap = {}
  end
  object JumpList1: TJumpList
    Enabled = True
    ApplicationID = 'gDailyLog'
    CustomCategories = <>
    TaskList = <
      item
        FriendlyName = 'Jump List Item 1'
      end>
    Left = 272
    Top = 184
  end
  object Taskbar1: TTaskbar
    TaskBarButtons = <>
    TabProperties = []
    Left = 280
    Top = 192
  end
end
