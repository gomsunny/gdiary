program gdiary;

uses
  Vcl.Forms,
  v.main in 'v.main.pas' {vMain},
  mMemoHelper in 'gs2lib\source\utils.vcl\mMemoHelper.pas',
  mListViewHelper in 'gs2lib\source\utils.vcl\mListViewHelper.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TvMain, vMain);
  Application.Run;
end.
