unit v.main;

interface

uses
  mvw.vForm, Spring.Collections,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ExtCtrls, System.Win.TaskbarCore, Vcl.Taskbar,
  RzStatus, System.ImageList, Vcl.ImgList, PngImageList, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan, RzPanel, RzButton, Vcl.StdCtrls, RzSplit, RzListVw, AdvListV,
  Vcl.JumpList;

type
  TvMain = class(TvForm)
    RzSplitter1: TRzSplitter;
    RzStatusBar: TRzStatusBar;
    RzToolbar1: TRzToolbar;
    RzVersionInfoStatus1: TRzVersionInfoStatus;
    Memo: TMemo;
    ActionManager1: TActionManager;
    ActionCurrentTime: TAction;
    ButtonNew: TRzToolButton;
    RzSpacer1: TRzSpacer;
    Imgs: TPngImageList;
    ActionNew: TAction;
    ActionSave: TAction;
    RzToolButton1: TRzToolButton;
    RzVersionInfo1: TRzVersionInfo;
    TimerMemoChanged: TTimer;
    ImgsDisabled: TPngImageList;
    ListView: TAdvListView;
    StatusIme: TRzStatusPane;
    RzKeyStatus1: TRzKeyStatus;
    JumpList1: TJumpList;
    Taskbar1: TTaskbar;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);

    procedure ListViewClick(Sender: TObject);
    procedure MemoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure MemoChange(Sender: TObject);
    procedure ActionCurrentTimeExecute(Sender: TObject);
    procedure ActionNewExecute(Sender: TObject);
    procedure ActionSaveExecute(Sender: TObject);
    procedure TimerMemoChangedTimer(Sender: TObject);
    procedure MemoKeyPress(Sender: TObject; var Key: Char);
  private
    type
      TBraketStatus = ( bsNone, bsEmpty, bsContents );
  private
    FBraketStatus: TBraketStatus;
    FShiftEnter: Boolean;
    FTabCnt: Integer;
    FGroups: IDictionary<String, Integer>;
    FMemoChanedTime: TDateTime;
    procedure UpdateFolders;
    procedure OnAppMessage(var AMsg: TMsg; var Handled: Boolean);
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  DateUtils, mDateTimeHelper, mFontHelper, IOUtils, mMemoHelper, Math, Types, UITypes, mListViewHelper, Imm, StrUtils,
  CodeSiteLogging
  ;

function IdImeHanMode: boolean;
var
  LMode: HIMC;
  LConversion, LSentence: Dword;
begin
  LMode := ImmGetContext(Application.Handle);
  ImmGetConversionStatus(LMode, LConversion, LSentence);
  Result := LConversion = IME_CMODE_HANGEUL;
end;

procedure TvMain.ActionCurrentTimeExecute(Sender: TObject);
begin
  Memo.Lines.BeginUpdate;
  try
    Memo.InsertCaretPos(Now.ToString('HH:NN'));
  finally
    Memo.Lines.EndUpdate;
  end;
end;

procedure TvMain.ActionNewExecute(Sender: TObject);
var
  LPath, LFile: String;
begin
  LPath := TPath.Combine(TPath.GetFullPath('.\dlog'), Now.ToString('YYYYMM'));
  if not TDirectory.Exists(LPath) then
    TDirectory.CreateDirectory(LPath);

  LFile := TPath.Combine(LPath, Now.ToString('DD') + '.txt');
  if not TFile.Exists(LFile) then
    TFile.Create(LFile).Free;

  UpdateFolders;
end;

procedure TvMain.ActionSaveExecute(Sender: TObject);
var
  LFile: string;
  LItem: TListItem;
  LGroup: TListGroup;
begin
  LItem := ListView.Selected;
  if not Assigned(LItem) then
    Exit;

  LGroup := ListView.Groups[LItem.GroupID];
  LFile := TPath.Combine('.\dlog\' +LGroup.SubTitle, LItem.SubItems[0] + '.txt');
  Memo.Lines.SaveToFile(LFile);
  ActionSave.Enabled := False;
end;

procedure TvMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  LEnalbed: Boolean;
begin
  LEnalbed := TimerMemoChanged.Enabled;
  TimerMemoChanged.Enabled := False;
  if ActionSave.Enabled then
    CanClose := MessageDlg('이 저장되지 않았습니다. 그래도 종료하시겠습니까 ?', mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrYes;

  if not CanClose then
    TimerMemoChanged.Enabled := LEnalbed;
end;

procedure TvMain.FormCreate(Sender: TObject);
begin
  FGroups := TCollections.CreateDictionary<String, Integer>;

  TFixedWidthFont.AssingToCtrls(Controls<TControl>(RzSplitter1));
  Memo.WantTabs := True;
  StatusIme.Caption := IfThen(IdImeHanMode, 'Kor', 'Eng');
  Application.OnMessage := OnAppMessage;

  ActionNew.Execute;
end;

procedure TvMain.ListViewClick(Sender: TObject);
var
  LFile: string;
  LItem: TListItem;
  LGroup: TListGroup;
begin
  LItem := ListView.Selected;
  if not Assigned(LItem) then
    Exit;

  LGroup := ListView.Groups[LItem.GroupID];
  LFile := TPath.Combine('.\dlog\' +LGroup.Subtitle, LItem.SubItems[0] + '.txt');
  Caption := Format('g Daily Log - %s년 %s월 %s일',
    [LGroup.Subtitle.Substring(0, 4), LGroup.Subtitle.Substring(4), LItem.SubItems[0]]);

  Memo.OnChange := nil;
  Memo.Lines.BeginUpdate;
  try
    Memo.Lines.LoadFromFile(LFile);
  finally
    Memo.Lines.EndUpdate;
    Memo.OnChange := MemoChange;
  end;
end;

procedure TvMain.MemoChange(Sender: TObject);
begin
  if (FTabCnt > 0) or (FShiftEnter) or (FBraketStatus <> bsNone) then
  begin
    Memo.OnChange := nil;
    Memo.Lines.BeginUpdate;
    try
      if FShiftEnter then
      begin
        FShiftEnter := False;
        Memo.InsertCaretPos(Now.ToString('HH:NN')+#9);
      end;


      case FBraketStatus of
        bsNone:
          while FTabCnt > 0 do
          begin
            Dec(FTabCnt);
            Memo.InsertCaretPos(#9);
          end;

        bsEmpty:
        begin
          Memo.MoveCaret(-1, 0);
          Memo.InsertCurrentCarret(' ');
          Memo.MoveCaretLineEnd;
          Memo.InsertCurrentCarret(' ');
        end;

        bsContents: ;
      end;
      FBraketStatus := bsNone;
    finally
      Memo.Lines.EndUpdate;
      Memo.OnChange := MemoChange;
    end;
  end;
  ActionSave.Enabled := True;
  FMemoChanedTime := Now;
  TimerMemoChanged.Enabled := True;
end;

procedure TvMain.MemoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  LPos: TPoint;
  i: Integer;
  LLine: string;
begin
//  if Key = VK_PROCESSKEY then
//    StatusIme.Caption := IfThen(IdImeHanMode, 'Kor', 'Eng');

  if (Key = VK_RETURN) then
  begin
    FShiftEnter := ssShift in Shift;
    if FShiftEnter then
      Exit;

    FTabCnt := 0;
    LPos := Memo.CaretPos;
    LLine := Memo.Lines[LPos.Y];
    i := LLine.IndexOf(#9);
    while i > -1 do
    begin
      Inc(FTabCnt);
      i := IfThen(LLine.Chars[i +1] = #9, i +1, -1);
    end;
  end;
end;

procedure TvMain.MemoKeyPress(Sender: TObject; var Key: Char);
var
  LPos: TPoint;
  LLine: string;
begin
  LPos := Memo.CaretPos;
  if (LPos.X > 1) then
  begin
    LLine := Memo.Lines[LPos.Y];
    if (Key = ']') and (LLine.Chars[LPos.X -1] = '[') then
      FBraketStatus := bsEmpty
    else if (LLine.Chars[LPos.X -1] = '[') and (LPos.X +1 < LLine.Length) and (LLine.Chars[LPos.X +1] = ']') then
      FBraketStatus := bsContents
  end
end;

procedure TvMain.OnAppMessage(var AMsg: TMsg; var Handled: Boolean);
begin
  if AMsg.message = $7fff then
    StatusIme.Caption := IfThen(IdImeHanMode, 'Kor', 'Eng');
end;

procedure TvMain.TimerMemoChangedTimer(Sender: TObject);
begin
  if (FMemoChanedTime.SecondsBetween(Now) > 1) and ActionSave.Enabled then
  begin
    TimerMemoChanged.Enabled := False;
    ActionSave.Execute;
  end;
end;

procedure TvMain.UpdateFolders;
var
  LFile, LGroupName, LRoot, LPath, LYmd: String;
  LItem: TListItem;
  LGroup: TListGroup;
  function LVCompare(Group1_ID: Integer; Group2_ID: Integer; pvData: Pointer): Integer; stdcall;
  var
    LGroup1, LGroup2: TListGroup;
  begin
    Result := 0;
    LGroup1 := TListGroups(pvData).FindItemByGroupID(Group1_ID);
    LGroup2 := TListGroups(pvData).FindItemByGroupID(Group2_ID);
    if Assigned(LGroup1) and Assigned(LGroup2) then
      Result := -CompareText(LGroup1.Header, LGroup2.Header)
  end;
begin
  ListView.Items.BeginUpdate;
  try
    ListView.ClearInit(1, 1);
    LRoot := TPath.Combine(TDirectory.GetCurrentDirectory, 'dlog');
    for LPath in TDirectory.GetDirectories(LRoot) do
    begin
      for LFile in TDirectory.GetFiles(LPath, '*.txt') do
      begin
        LYmd := LPath.Substring((LRoot+'\').Length) + TPath.GetFileNameWithoutExtension(LFile);
        LGroupName :=  Format('%d주', [TDateTime.FmtYMD(LYmd).WeekOfTheYear]);
        if FGroups.ContainsKey(LGroupName) then
          LGroup := ListView.Groups[FGroups[LGroupName]]
        else
        begin
          LGroup := ListView.Groups.Add;
          LGroup.Header := LGroupName;
          LGroup.Subtitle := LYmd.Substring(0, 6);
          LGroup.Index := ListView.Groups.Count -1;
          FGroups.Add(LGroupName, LGroup.Index);
        end;
        LItem := ListView.Items.Add;
        LItem.GroupID := LGroup.Index;
        LItem.Caption := '';
        LItem.SubItems.Add(TPath.GetFileNameWithoutExtension(LFile));
      end;
    end;
    ListView.SortColumn := 1;
    ListView.Sort;
    ListView.SortWithGroups(@LVCompare);
  finally
    ListView.Items.EndUpdate;
  end;
end;

end.
